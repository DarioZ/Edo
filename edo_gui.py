import sys
import numpy as np
import tkinter as tk
from tkinter import ttk

N = tk.N
S = tk.S
E = tk.E
W = tk.W


def primesfrom2to(n):
    # http://stackoverflow.com/questions/2068372/fastest-way-to-list-all-primes-below-n-in-python/3035188#3035188
    """ Input n>=6, Returns a array of primes, 2 <= p < n """
    sieve = np.ones(n/3 + (n % 6 == 2), dtype=np.bool)
    sieve[0] = False
    for i in range(int(n**0.5/3) + 1):
        if sieve[i]:
            k = 3 * i + 1 | 1
            sieve[((k*k)/3)::2*k] = False
            sieve[(k * k + 4 * k - 2 * k * (i & 1))/3::2*k] = False
    return np.r_[2, 3, ((3 * np.nonzero(sieve)[0] + 1) | 1)]


def primes_file(number):
    """Stampa in un file i primi minori di number usando primesfrom2to"""
    print("Creazione lista primi<{}..".format(number+1))
    primes = primesfrom2to(number)
    filename = "primes_to_" + str(number) + ".txt"
    with open(filename, "w") as f:
        for n in primes:
            f.write("{}\n".format(n))
    print("Creata lista di primi<{} in {}!\n".format(
        number + 1, filename))


def function(number):
    """Crea un file con i valori primi di 11n +6"""
    # TODO: ampliare a qualunque tipo di funzione per n
    print("Svolgimento funzione 11n + 6 "
          "per n<{}...".format(number + 1))
    primes = primesfrom2to(11*number + 7)
    filename = "function_values_to_" + str(number) + ".txt"
    with open(filename, "w") as f:
        f.write("Valore di n\t\t\t\tRisultato di 11n + 6\t\t\tCheck\n\n")
        prim = 0
        nprim = 0
        for n in range(0, number):
            if n % 2 != 0:
                if n % 3 != 0:
                    if (n-9) % 10 != 0:
                        result = (11 * n) + 6
                        if result in primes:
                            prime_check = "PRIMO"
                            prim += 1
                        else:
                            prime_check = "NON PRIMO"
                            nprim += 1
                        f.write("{num}\t\t\t\t\t{f_num}\t\t\t\t\t\
                                {check}\n".format(num=n, f_num=result,
                                                  check=prime_check))
    print("Completato!")
    print("Statistiche:")
    print("Numeri primi ottenuti:{}".format(prim))
    print("Numeri no-primi ottenuti:{}".format(nprim))
    print("Risultato percentuale:{}%".format(
        (float(prim)/(prim + nprim))*100))


class Application(ttk.Frame):
    def __init__(self, master=None):
        self.number = tk.StringVar()
        ttk.Frame.__init__(self, master, padding="3")
        self.grid(column=0, row=0, sticky=(N, W, E, S))
        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)
        self.setStyle()
        self.createWidgets()
        sys.stdout.write = self.redirectText

    def setStyle(self):
        """Imposta stile e padding di tutti i widget sullo schermo"""
        style = ttk.Style()
        style.configure("TButton", padding=6, relief="flat", background="#ccc")
        style.configure("TEntry", padding=6, relief="flat", background="#fff")
        style.configure("TText", padding=6, relief="flat", background="#fff")
        # style.configure("Horizontal.TProgressbar", padding=6, relief="flat",
        #                background="#ccc")
        # for child in self.winfo_children():
        #    child.grid_configure(padx=5, pady=5)

    def createWidgets(self):
        """Crea tutti i widget sullo schermo"""
        # Widget entry per input numero
        self.enter = ttk.Entry(self, textvariable=self.number)
        self.enter.grid(row=0, columnspan=2, pady=5)
        self.enter.focus()

        # Widget bottone
        self.calculatef = ttk.Button(
            self, text="Calcola i valori della funzione",
            command=self.calcolaf)
        self.calculatef.grid(row=1)

        # Widget bottone
        self.calculatep = ttk.Button(
            self, text="Calcola i primi da 0 a 11n+6 incluso",
            command=self.calcolap)
        self.calculatep.grid(row=1, column=1)

        # Widget tk.Text che mostra lo stdout
        self.logwidget = tk.Text(self)
        self.logwidget.grid(row=2, rowspan=5, columnspan=2)

        # Progress bar disabilitata perché inutile il programma è troppo veloce
        # self.progress = ttk.Progressbar(self, mode="indeterminate", variable=0)
        # self.progress.grid(row=7, columnspan=2)

    def calcolaf(self):
        """Redirect a function"""
        try:
            number = int(self.number.get())
            # self.progress.start()
            function(number)
            # self.progress.stop()
        except ValueError:
            print("Scrivi un numero naturale\n")

    def calcolap(self):
        """Redirect a primes_file"""
        try:
            number = int(self.number.get())
            # self.progress.start()
            print("11*{} + 6 fa {}".format(number, 11*number + 6))
            primes_file(11*number + 7)
            # self.progress.stop()
        except ValueError:
            print("Scrivi un numero naturale\n")
            pass

    def redirectText(self, str):
        # http://stackoverflow.com/questions/12351786/python-converting-cli-to-gui
        """Ridireziona lo stdout al widget self.logwidget, vedere tk.Text"""
        self.logwidget.configure(state="normal")
        self.logwidget.insert("end", str, "stdout")
        self.logwidget.configure(state="disabled")


root = tk.Tk()
app = Application(master=root)
root.title("Calcolatore di primi di tipo 11n + 6")
app.mainloop()
