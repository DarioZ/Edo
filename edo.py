# -*- coding: utf-8 -*-
from __future__ import print_function
import numpy as np


def primesfrom2to(n):
    # http://stackoverflow.com/questions/2068372/fastest-way-to-list-all-primes-below-n-in-python/3035188#3035188
    """ Input n>=6, Returns a array of primes, 2 <= p < n """
    sieve = np.ones(n/3 + (n % 6 == 2), dtype=np.bool)
    sieve[0] = False
    for i in xrange(int(n**0.5)/3 + 1):
        if sieve[i]:
            k = 3 * i + 1 | 1
            sieve[((k*k)/3)::2*k] = False
            sieve[(k * k + 4 * k - 2 * k * (i & 1))/3::2*k] = False
    return np.r_[2, 3, ((3 * np.nonzero(sieve)[0] + 1) | 1)]


# Inserimento numero
while True:
    try:
        number = raw_input("Inserisci un numero naturale:")
        number = int(number)
        break
    except ValueError:
        print("Non hai inserito un numero naturale. \
            Inserisci un numero naturale")

# Creazione lista primi primes in primes.txt
print("Creazione lista primi<{}...".format(number+1))
primes = primesfrom2to(11*number + 7)
f2 = open("primes.txt", "w")
for n in primes:
    f2.write("{}\n".format(n))
f2.close()
print("Creata lista di primi<{} in primes.txt!".format(number + 1))

# Svolgimento del programma principale
print("Svolgimento funzione 11n + 6 per n<{}...".format(number + 1))
f = open("numbers.txt", "w")
f.write("Valore di n\t\t\t\tRisultato di 11n + 6\t\t\tCheck\n\n")
prim = 0
nprim = 0
for n in range(0, number):
    if n % 2 != 0:
        if n % 3 != 0:
            if (n-9) % 10 != 0:
                result = (11 * n) + 6
                if result in primes:
                    prime_check = "PRIMO"
                    prim += 1
                else:
                    prime_check = "NON PRIMO"
                    nprim += 1
                f.write("{num}\t\t\t\t\t{f_num}\t\t\t\t\t{check}\n".format(
                    num=n, f_num=result, check=prime_check))
f.close()

print("Completato!")
print("Statistiche:")
print("Numeri primi ottenuti:{}".format(prim))
print("Numeri non primi ottenuti:{}".format(nprim))
print("Risultato percentuale:{}%".format((float(prim)/(prim + nprim))*100))
