import sys
import numpy as np
from py_expression_eval import Parser
import tkinter as tk
from tkinter import ttk

# changelog: aggiunta possibilità di valutare funzioni qualunque
# TODO: possibilità di evitare alcuni divisori
# TODO: grafico dei valori e confronto con grafico dei primi


def primesfrom2to(n):
    # http://stackoverflow.com/questions/2068372/fastest-way-to-list-all-primes-below-n-in-python/3035188#3035188
    """ Input n>=6, Returns a array of primes, 2 <= p < n """
    sieve = np.ones(n/3 + (n % 6 == 2), dtype=np.bool)
    sieve[0] = False
    for i in range(int(n**0.5/3) + 1):
        if sieve[i]:
            k = 3 * i + 1 | 1
            sieve[((k*k)/3)::2*k] = False
            sieve[(k * k + 4 * k - 2 * k * (i & 1))/3::2*k] = False
    return np.r_[2, 3, ((3 * np.nonzero(sieve)[0] + 1) | 1)]


def primes_file(number):
    """Stampa in un file i primi minori di number usando primesfrom2to"""
    print("Creazione lista primi<{}..".format(number+1))
    primes = primesfrom2to(number)
    filename = "primes_to_" + str(number) + ".txt"
    with open(filename, "w") as f:
        for n in primes:
            f.write("{}\n".format(n))
    print("Creata lista di primi<{} in {}!\n".format(
        number + 1, filename))


def function(expression, number):
    """Crea un file con i valori primi della funzione nella str expression"""
    parser = Parser()
    expr = parser.parse(expression)
    var = expr.variables()
    max_value = expr.evaluate({var[0]: number})
    print("Svolgimento funzione {} "
          "per n<{}...".format(expr.toString(), number + 1))
    primes = primesfrom2to(max_value)
    filename = "function_values_of_" + expr.simplify({}).toString() \
               + "_to_" + str(number) + ".txt"
    with open(filename, "w") as f:
        f.write("Valore di {}\t\t\t\tRisultato di {}\t\t\t\tCheck\n\n".format(
                                         var[0], expr.simplify({}).toString()))
        prim = 0
        nprim = 0
        for n in range(0, number):
            result = expr.evaluate({var[0]: n})
            if result in primes:
                prime_check = "PRIMO"
                prim += 1
            else:
                prime_check = "NON PRIMO"
                nprim += 1
            f.write("{num}\t\t\t\t\t{f_num}\t\t\t\t\t\
                                {check}\n".format(num=n, f_num=result,
                                                  check=prime_check))
    print("Completato!")
    print("Statistiche:")
    print("Numeri primi ottenuti:{}".format(prim))
    print("Numeri no-primi ottenuti:{}".format(nprim))
    print("Risultato percentuale:{}%".format(
        (float(prim)/(prim + nprim))*100))


class Application(ttk.Frame):
    def __init__(self, master=None):
        self.number = tk.StringVar()
        self.expression = tk.StringVar()
        self.divisori = tk.StringVar()

        ttk.Frame.__init__(self, master, padding="3")
        self.grid(row=0, column=0, sticky="nsew")
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)
        self.widgetsframe = ttk.Frame(self)
        self.widgetsframe.grid(row=0, column=0)
        self.setStyle()
        self.createWidgets()
        sys.stdout.write = self.redirectText

    def setStyle(self):
        """Imposta stile e padding di tutti i widget sullo schermo"""
        style = ttk.Style()
        style.configure("TButton", padding=6, relief="flat", background="#ccc")
        style.configure("TEntry", padding=6, relief="flat", background="#fff")
        style.configure("TText", padding=6, relief="flat", background="#fff")
        # style.configure("Horizontal.TProgressbar", padding=6, relief="flat",
        #                background="#ccc")
        # for child in self.winfo_children():
        #    child.grid_configure(padx=5, pady=5)

    def createWidgets(self):
        """Crea tutti i widget sullo schermo"""
        # Widget per input espressione
        self.expr_label = ttk.Label(self.widgetsframe,
                                    text="Inserisci la funzione").pack(pady=5)
        self.expr_enter = ttk.Entry(self.widgetsframe,
                                    textvariable=self.expression)
        self.expr_enter.pack(pady=5)
        self.expr_enter.focus()

        # # Widget entry per input numero
        # self.n_label = ttk.Label(self.widgetsframe,
        #                          text="per n minore di").pack(pady=5)
        # self.n_enter = ttk.Entry(self.widgetsframe,
        #                          textvariable=self.number).pack(pady=5)

        # TODO: Widget entry per divisori

        # Widget bottone
        self.calculatef = ttk.Button(
            self.widgetsframe, text="Calcola i valori della funzione",
            command=self.calcolaf).pack(pady=5)

        # Widget bottone
        self.calculatep = ttk.Button(
            self.widgetsframe, text="Calcola i primi da 0 a 11n+6 incluso",
            command=self.calcolap).pack(pady=5)

        # Widget tk.Text che mostra lo stdout
        self.logwidget = tk.Text(self, width=60, height=15)
        self.logwidget.grid(row=0, column=1)
        self.logwidget.configure(state="disabled")

    def calcolaf(self):
        """Redirect a function"""
        try:
            number = int(self.number.get())
            try:
                expression = self.expression.get()
                function(expression, number)
            except Exception:
                print("Scrivi la funzione correttamente")
        except ValueError:
            print("Scrivi un numero naturale\n")

    # def calcolap(self):
    #     """Redirect a primes_file"""
    #     try:
    #         number = int(self.number.get())
    #         print("11*{} + 6 fa {}".format(number, 11*number + 6))
    #         primes_file(11*number + 7)
    #     except ValueError:
    #         print("Scrivi un numero naturale\n")
    #         pass

    def redirectText(self, str):
        # http://stackoverflow.com/questions/12351786/python-converting-cli-to-gui
        """Ridireziona lo stdout al widget self.logwidget, vedere tk.Text"""
        self.logwidget.configure(state="normal")
        self.logwidget.insert("end", str, "stdout")
        self.logwidget.configure(state="disabled")


root = tk.Tk()
app = Application(master=root)
root.title("Calcolatore di primi per ogni funzione di n")
app.mainloop()
