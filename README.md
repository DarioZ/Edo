# Edo - Calcolatore di primi per una f(x)
Calcolatore di numeri primi dati da una funzione f(x) per tutte le x da 0 a
x massimo. E' possibile eliminare dalle x tutti i numeri divisibili per un
numero n.

Basato su numpy con gui tkinter. La funzione che ritorna i primi è presa da
[StackOverflow](http://stackoverflow.com/questions/2068372/fastest-way-to-list-all-primes-below-n-in-python/3035188#3035188)

* `edo.py` è il primo prototipo del progetto per una f(x) predefinita nel
programma.
* `edo_gui3.py` è l'ultima versione del programma, necessita dei moduli `numpy`,
`tkinter` e `py_expression_eval` e ha una semplice GUI in cui è possibile
specificare la f(x), la x massima e i divisori delle x da non considerare.

Nel repository è incluso un `venv` creato su ambiente linux x64.